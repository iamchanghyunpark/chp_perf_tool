all: chp_perf.o chp_mem.o test libchpperf.so libchpperf.a

CFLAGS:=-Wall -fPIC -g
PREFIX:=$(HOME)/usr

chp_mem.o: chp_mem.c
	gcc ${CFLAGS} -c $<

chp_perf.o: chp_perf.c
	gcc ${CFLAGS} -c $< 

test: test.c chp_perf.o chp_mem.o
	gcc ${CFLAGS} -o $@ $^

libchpperf.so: chp_mem.o chp_perf.o
	gcc -shared -o $@ $^ -lc

libchpperf.a: chp_mem.o chp_perf.o
	ar rcs $@ $^

install: libchpperf.so libchpperf.a
	mkdir -p ${PREFIX}
	install -t ${PREFIX}/lib $^
	install --mode=0644 -t ${PREFIX}/include *.h

clean:
	-rm *.o
	-rm test
