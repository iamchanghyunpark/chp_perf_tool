#include "chp_mem.h"

/** Memory usage related functions **/
char proc_file[] = "/proc/self/status";

int startswith(const char *s, const char *prefix)
{
    return (strncmp(s, prefix, strlen(prefix)) == 0);
}

//Function from gem5 src/base/hostinfo.cc
long procInfo(const char *filename, const char *target)
{
    int  done = 0;
    char line[80];
    char format[80];
    long usage;

    FILE *fp = fopen(filename, "r");

    while (fp && !feof(fp) && !done) {
        if (fgets(line, 80, fp)) {
            if (startswith(line, target)) {
                snprintf(format, sizeof(format), "%s %%ld", target);
                sscanf(line, format, &usage);

                fclose(fp);
                return usage ;
            }
        }
    }

    if (fp)
        fclose(fp);

    return 0;
}

unsigned long getVMPeak() {
    return procInfo(proc_file, "VmPeak:");
}

unsigned long getVmPeakRSS() {
    return procInfo(proc_file, "VmHWM:");
}

unsigned long getHugeTlbPages() {
    return procInfo(proc_file, "HugetlbPages:");
}

void printMemUsage() {
    unsigned long vm = getVMPeak();
    unsigned long rss = getVmPeakRSS();
    unsigned long huge = getHugeTlbPages();
    unsigned long total = rss + huge;
    printf("Peak VM   usage:%'10lu kB,\t%6.2f MB,\t%5.2f GB\n",
            vm, (float)vm/1024, (float)vm/1024/1024);
    printf("Peak RSS  usage:%'10lu kB,\t%6.2f MB,\t%5.2f GB\n",
            rss, (float)rss/1024, (float)rss/1024/1024);
    printf("Peak Huge usage:%'10lu kB,\t%6.2f MB,\t%5.2f GB\n",
            huge, (float)huge/1024, (float)huge/1024/1024);
    printf("Peak phys usage:%'10lu kB,\t%6.2f MB,\t%5.2f GB\n",
            total, (float)total/1024, (float)total/1024/1024);
}

/** Pmap related functions **/
char filename_maps[] = "/proc/self/maps";

struct pmap_entry {
    unsigned long long start_addr;
    unsigned long long end_addr;
    char perms[5];
    char *backing_file;
};

struct pmap_entry *pmap_arr;
int pmap_arr_len;

void parse_pmap_entry(struct pmap_entry *pe, char *line)
{
    char tok_buf[1024];
    char *tok;
    char addrs[34];
    strncpy(tok_buf, line, 1024);

    //First is addrs
    tok = strtok(tok_buf, " \t");
    strncpy(addrs, tok, 34);

    //Second is permissions
    tok = strtok(NULL, " \t");
    strncpy(pe->perms, tok, 5);

    //Third, fourth and fifth are offset/device/inodes
    tok = strtok(NULL, " \t");
    tok = strtok(NULL, " \t");
    tok = strtok(NULL, " \t\n");

    tok = strtok(NULL, " \t\n");
    if(tok) {
        //Non empty
        int len = strlen(tok);
        pe->backing_file = (char *)malloc(len+1);
        if (!pe->backing_file) {
            perror("Allocating memory for backing_filename");
            return;
        }
        strncpy(pe->backing_file, tok, len+1);
    } else {
        pe->backing_file = NULL;
    }

    tok = strtok(addrs, "-");
    pe->start_addr = strtoull(tok, NULL, 16);
    tok = strtok(NULL, "-");
    pe->end_addr= strtoull(tok, NULL, 16);
}

void print_pmap_entry(struct pmap_entry *pe) {
    size_t size = pe->end_addr - pe->start_addr;
    printf("%12llx-%12llx (%#16lx, %7ld pages) %s %s\n",
            pe->start_addr, pe->end_addr, size, size/4096,
            pe->perms, pe->backing_file ? pe->backing_file : "");
}

void print_pmap_arr() {
    int i;
    for (i = 0; i < pmap_arr_len; i++) {
        print_pmap_entry(&pmap_arr[i]);
    }
}

int pmap_entry_is_heap_or_anon(struct pmap_entry *pe)
{
    if (!pe->backing_file)
        return 1;
    if (strcmp(pe->backing_file, "[heap]") == 0)
        return 1;
    return 0;
}

void read_pmap()
{
   char buf[1024];
   int lines = 0;

   FILE *fp = fopen(filename_maps, "r");

   if (!fp) {
       perror("Error opening pmaps file!");
       return;
   }

   while(fgets(buf, 1024, fp))
       lines++;

   fseek(fp, 0L, SEEK_SET);

   pmap_arr = (struct pmap_entry*)malloc(sizeof(*pmap_arr) * lines);
   if (!pmap_arr) {
       perror("Error allocating pmap array\n");
       fclose(fp);
       return;
   }

   lines = 0;
   while(fgets(buf, 1024, fp)) {
        parse_pmap_entry(&pmap_arr[lines], buf);
        lines++;
   }
   pmap_arr_len = lines;

   fclose(fp);
}

void count_per_pmap(struct pmap_entry *pe, size_t *np_cnt, size_t *p_4KB_cnt,
        size_t *p_2MB_cnt, int pm_fd, int kpf_fd)
{
    unsigned long long start_pg, end_pg;
    unsigned long long i;

    start_pg = pe->start_addr >> 12;
    end_pg = pe->end_addr >> 12;

    unsigned long long pagemap_read;
    unsigned long long kpageflag_read;

    for (i = start_pg; i < end_pg; i++) {
        ssize_t len;
        len = pread(pm_fd, &pagemap_read, 8, i*8);
        if (len != 8) {
            perror("Error reading from pagemap_fd");
            return;
        }

        unsigned long long isPresent = pagemap_read & (1ULL << 63);

        if (!isPresent) {
            (*np_cnt)++;
            continue;
        }

        if ((i % 512) != 0) { //Not aligned to 2MB. Won't be large page.
            (*p_4KB_cnt)++;
            continue;
        }

        unsigned long long pfn = pagemap_read & ((1ULL << 55)-1);
        len = pread(kpf_fd, &kpageflag_read, 8, pfn*8);
        if (len != 8) {
            perror("Error reading from kpageflags_fd");
            return;
        }

        unsigned long long isHuge = kpageflag_read & (1ULL << 17);
        unsigned long long isTHP = kpageflag_read & (1ULL << 22);

        if (isHuge || isTHP) {
            (*p_2MB_cnt)++;
            i += 511;
        } else {
            (*p_4KB_cnt)++;
        }
    }
}

void check_is_root()
{
    if (geteuid() != 0) {
        printf("Current effective user is not root. On recent kernels "
                "(post 4.0) detecting number of large pages will not work\n");
    }
}

char filename_pagemap[] = "/proc/self/pagemap";
char filename_kpageflags[] = "/proc/kpageflags";
void get2MB_4KBNumPages(size_t *count_2M, size_t *count_4K)
{
    struct pmap_entry* pmaps[10];
    int i,count = 0;

    int pagemap_fd, kpageflags_fd;

    size_t not_present = 0UL, present_4KB = 0UL, present_2MB = 0UL;

    check_is_root();

    pagemap_fd = open(filename_pagemap, O_RDONLY);
    if (pagemap_fd == -1) {
        perror("Error opening pagemap file");
        return;
    }

    kpageflags_fd = open(filename_kpageflags, O_RDONLY);
    if (kpageflags_fd == -1) {
        perror("Error opening kpageflags file. "
                "Make sure to run as sudo");
        close(pagemap_fd);
        return;
    }

    if (pmap_arr == NULL) {
        read_pmap();
    }

    for (i = 0; i < pmap_arr_len; i++) {
        size_t pages = (pmap_arr[i].end_addr - pmap_arr[i].start_addr)/4096;
        if (pages > 512 && pmap_entry_is_heap_or_anon(&pmap_arr[i])) {
            //Anon/heap regions greater than 2MB
            pmaps[count] = &pmap_arr[i];
            count++;
        }
    }

    printf("Looking at %d pmaps:\n", count);
    for (i =0; i < count; i++) {
        size_t np =0UL, p_4KB = 0UL, p_2MB = 0UL;
        count_per_pmap(pmaps[i], &np, &p_4KB, &p_2MB,
                pagemap_fd, kpageflags_fd);

        print_pmap_entry(pmaps[i]);
        printf("Not present: %ld, 4KB: %ld, 2MB: %ld, total present: %ld\n",
                np, p_4KB, p_2MB, p_4KB + p_2MB*512);

        not_present += np;
        present_4KB += p_4KB;
        present_2MB += p_2MB;
    }

    printf("Total: Not present: %ld, 4KB: %ld, 2MB: %ld, total present: %ld\n",
            not_present, present_4KB, present_2MB, present_4KB+ present_2MB*512);
    close(pagemap_fd);
    close(kpageflags_fd);

    if (count_2M)
        *count_2M = present_2MB;
    if (count_4K)
        *count_4K = present_4KB;
}
