#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#ifdef __cplusplus
extern "C" {
#endif

// Memory usage related functions
void printMemUsage();
unsigned long getVMPeak();
unsigned long getVmPeakRSS();
unsigned long getHugeTlbPages();

// Pmap related functions
void read_pmap();
void print_pmap_arr();

void get2MB_4KBNumPages(size_t *count_2M, size_t *count_4K);

#ifdef __cplusplus
};
#endif
