#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#ifdef __cplusplus
extern "C" {
#endif

struct perf_struct;

struct counter_pair {
    unsigned long id;
    unsigned long val;
};

struct counter_arr {
    size_t nr;
    struct counter_pair vals[];
};

struct perf_struct *init_perf(size_t nr);
struct counter_arr *init_counters(struct perf_struct *perf);
void reset_counter(struct perf_struct *perf);
void start_counter(struct perf_struct *perf);
void stop_counter(struct perf_struct *perf);
void read_counter(struct perf_struct *perf, struct counter_arr *ctrs);
void print_aggregate_counter(struct counter_arr *ctrs);

#ifdef __cplusplus
};
#endif
