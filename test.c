#include <stdio.h>
#include "chp_perf.h"
#include "chp_mem.h"
#include <sys/types.h>
#include <unistd.h>

int main()
{
    volatile int test_var = 0;

    struct perf_struct *perf;

    char *thp_buf;
    size_t buf_size = 2*1024*1024*16;
    thp_buf = malloc(buf_size);

    perf = init_perf(3);
    reset_counter(perf);
    start_counter(perf);

    for (int i = 0; i < buf_size; i++) {
        thp_buf[i] = test_var++;
    }

    stop_counter(perf);
    read_counter(perf, NULL);

    printMemUsage();

    read_pmap();
    print_pmap_arr();
    get2MB_4KBNumPages(NULL, NULL);

    printf("Also printing pmap for reference\n");
    pid_t pid = getpid();
    char cmd[1024];
    sprintf(cmd, "cat /proc/%u/maps", pid);
    FILE *pp = popen(cmd, "r");
    char buf[1024];
    while(fgets(buf, 1024, pp)) {
        printf("%s", buf);
    }

    pclose(pp);
    return 0;
}
